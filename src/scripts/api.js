import * as Geo from './Geo'

export const getForecast = async () => {
    const position = await Geo.getCurrentPosition();
    const apiKey = 'bb6f054b4a6ff353878fc456cd28d499';
    const lon = position.coords.longitude;
    const lat = position.coords.latitude;

    const response = await fetch(`http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&APPID=${apiKey}&units=metric`);
    const json = await response.json();

    return {
        city: json.city.name,
        state: json.city.country,
        forecast: json.list.map( (day) => ({
            date: day.dt_txt,
            icon: day.weather[0].icon,
            high: day.main.temp_max,
            low: day.main.temp_min
        }))
    }
}